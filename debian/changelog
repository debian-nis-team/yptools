yp-tools (4.2.3-4) unstable; urgency=medium

  * Added a b-d on libnls-dev.
    (closes: #1065162)
  * Policy bumped to 4.6.2. No changes.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Fri, 01 Mar 2024 20:21:08 +0000

yp-tools (4.2.3-3) unstable; urgency=medium

  * One more change in gnuhurd patch.
  * Change /sbin/unix_chkpwd as suid root at postinst time. See #155583. 
  * Changed dependency on ybind-mt.
  * Moved conffile to ucf management to cope with nis package conffiles.
  * Now using dh-exec, added to b-d.
  * Pre-check availability of ucf before running in postrm.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Sat, 23 Jan 2021 15:22:47 +0100

yp-tools (4.2.3-2) unstable; urgency=medium

  * Added /var/yp/nicknames to conffiles.
  * Changed a bit long description in d/control.
  * Removed obsolete patches in d/patches. 
  * Removed an extra space in Maintainer field in d/control. 
  * Hardening building activated and cleaned up d/rules.
  * Added a gnuhurd patch for usual PATH_MAX missing.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Thu, 31 Dec 2020 15:14:25 +0100

yp-tools (4.2.3-1) unstable; urgency=medium

  * New maintainer, thanks Mark Brown.
  * New upstream release. All patches but one already merged or applied to 
    code now stripped.
  * Policy bumped to 4.5.1.
  * Debhelper compat moved to 13.
  * debian/copyright revised.
  * Added Vcs-* fields to debian/control.
  * Removed autotools-dev deprecated b-d.
  * Updated debian/watch to consult github upstream repo.
  * Removed libnis and libnis-dev pkgs not more distributed by upstream.
  * Added lintian-overrides file for known warning.
  * Added a patch to generate missing man for yptest.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Wed, 30 Dec 2020 11:29:04 +0100

yp-tools (3.3-5.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS for RES_USE_INET6. (Closes: #954587)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Mon, 30 Mar 2020 18:58:41 +0100

yp-tools (3.3-5.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix ftbfs. (Closes: #897890)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Thu, 27 Feb 2020 21:00:19 +0000

yp-tools (3.3-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/patches:
    - Add patch to fix FTBFS with GCC 7. (Closes: #853714)
    - Add patch to fix FTBFS on architectures with strict alignment
      requirements. (Closes: #836021)

 -- James Cowgill <jcowgill@debian.org>  Thu, 22 Mar 2018 14:55:46 +0000

yp-tools (3.3-5) unstable; urgency=low

  * Don't bother installing the domainname utilities (closes: #812532).

 -- Mark Brown <broonie@debian.org>  Sun, 23 Oct 2016 00:15:55 +0100

yp-tools (3.3-4) unstable; urgency=low

  * Remove unused UDPTIMEOUT definition.  Why do people think -Werror is
    a good idea?  (closes: #831122).

 -- Mark Brown <broonie@debian.org>  Tue, 09 Aug 2016 14:53:16 +0100

yp-tools (3.3-3) unstable; urgency=low

  * Fix ypclnt.h refrence in yp_prot.h.

 -- Mark Brown <broonie@debian.org>  Sun, 03 Jul 2016 16:15:22 +0200

yp-tools (3.3-2) unstable; urgency=low

  * Install headers for libnis, rename for libc compatiblity.

 -- Mark Brown <broonie@debian.org>  Sun, 03 Jul 2016 15:09:53 +0200

yp-tools (3.3-1) unstable; urgency=low

  * Split out of the existing NIS package since having multiple upstream
    packages in one binary isn't ideal.
  * New upstream version which also adds the new libnis1 library.

 -- Mark Brown <broonie@debian.org>  Sat, 07 Nov 2015 12:47:34 +0000
